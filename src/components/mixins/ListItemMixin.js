import { mapActions } from 'vuex'
import SliderInput from '@components/SliderInput'
export default {
  components: { SliderInput },
  props: {
    item: { type: Object, default: () => ({}) },
  },
  methods: {
    ...mapActions({ updateCart: 'store/updateCart' }),
  },
}
