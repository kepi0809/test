export default {
  data() {
    return {
      numberFormat: new Intl.NumberFormat('de-DE', {
        style: 'currency',
        currency: 'EUR',
        minimumFractionDigits: 2,
      }),
    }
  },
}
