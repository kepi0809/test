import NavBar from './nav-bar'
import AnimatedImage from './AnimatedImage'

describe('@components/nav-bar', () => {
  it('renders a Child component', () => {
    const wrapper = shallowMount(
      NavBar,
      createComponentMocks({
        store: {
          store: {
            state: {
              items: [
                {
                  id: '1f85ec65-c1f2-4adc-89e1-192637aaef6e',
                  name: 'Taschenlampe, Halogen, rot',
                  image: {
                    orig:
                      'https://images.unsplash.com/photo-1561916960-dea3b9b0355a?ixlib=rb-1.2.1&auto=format&fit=crop&w=640&q=80',
                    thumb:
                      'https://images.unsplash.com/photo-1561916960-dea3b9b0355a?ixlib=rb-1.2.1&auto=format&fit=crop&w=480&h=480&q=80',
                  },
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                  stock: 100,
                  price: { currency: 'EUR', value: 2995 },
                  cart: { amount: 1, totalCost: 2995 },
                  offset: { top: 32, left: 723 },
                },
              ],
            },
            getters: {
              cart: () => ({
                items: [
                  {
                    id: '3bcdd547-388c-4d6d-848c-a17b1414fe9c',
                    name: 'Fahrrad, grau',
                    image: {
                      orig:
                        'https://images.unsplash.com/photo-1485965120184-e220f721d03e?ixlib=rb-1.2.1&auto=format&fit=crop&w=640&q=80',
                      thumb:
                        'https://images.unsplash.com/photo-1485965120184-e220f721d03e?ixlib=rb-1.2.1&auto=format&fit=crop&w=480&h=480&q=80',
                    },
                    description:
                      'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                    stock: 2,
                    price: { currency: 'EUR', value: 49900 },
                    cart: { amount: 2, totalCost: 99800 },
                    offset: { top: 32, left: 345 },
                  },
                  {
                    id: '1f85ec65-c1f2-4adc-89e1-192637aaef6e',
                    name: 'Taschenlampe, Halogen, rot',
                    image: {
                      orig:
                        'https://images.unsplash.com/photo-1561916960-dea3b9b0355a?ixlib=rb-1.2.1&auto=format&fit=crop&w=640&q=80',
                      thumb:
                        'https://images.unsplash.com/photo-1561916960-dea3b9b0355a?ixlib=rb-1.2.1&auto=format&fit=crop&w=480&h=480&q=80',
                    },
                    description:
                      'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                    stock: 100,
                    price: { currency: 'EUR', value: 2995 },
                    cart: { amount: 1, totalCost: 2995 },
                    offset: { top: 32, left: 723 },
                  },
                ],
                totalItems: 3,
                totalCost: 102795,
              }),
            },
          },
        },
      })
    )
    expect(wrapper.find(AnimatedImage).exists()).toBe(true)
  })
})
