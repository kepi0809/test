import AnimatedImage from './AnimatedImage'

describe('@components/AnimatedImage', () => {
  it('exports a valid component', () => {
    expect(AnimatedImage).toBeAComponent()
  })
})
