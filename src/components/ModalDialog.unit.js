import ModalDialog from './ModalDialog'

describe('@components/ModalDialog', () => {
  it('exports a valid component', () => {
    expect(ModalDialog).toBeAComponent()
  })
})
