import ListingItem from './ListingItem'

describe('@components/ListingItem', () => {
  it('exports a valid component', () => {
    expect(ListingItem).toBeAComponent()
  })
})
