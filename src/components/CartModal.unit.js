import CartModal from './CartModal'

describe('@components/CartModal', () => {
  it('exports a valid component', () => {
    expect(CartModal).toBeAComponent()
  })
})
