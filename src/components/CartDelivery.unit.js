import CartDelivery from './CartDelivery'

describe('@components/CartDelivery', () => {
  it('exports a valid component', () => {
    expect(CartDelivery).toBeAComponent()
  })
})
