import CartListItem from './CartListItem'

describe('@components/CartListItem', () => {
  it('exports a valid component', () => {
    expect(CartListItem).toBeAComponent()
  })
})
