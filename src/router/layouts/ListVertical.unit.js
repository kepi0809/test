import ListVerticalLayout from './ListVertical'

describe('@layouts/ListVertical', () => {
  it('renders its content', () => {
    const slotContent = '<p>Hello!</p>'
    const { element } = shallowMount(ListVerticalLayout, {
      slots: {
        detail: slotContent,
      },
    })
    expect(element.innerHTML).toContain(slotContent)
  })
})
