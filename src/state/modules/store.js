import axios from 'axios'
import Vue from 'vue'

export const state = {
  items: [],
}

export const mutations = {
  SET_ITEMS(state, newValue) {
    state.items = newValue
  },

  SET_ITEM(state, [newItem, index]) {
    Vue.set(state.items, index, newItem)
  },

  SET_OFFSET(state, { offset, index }) {
    Vue.set(state.items, index, { ...state.items[index], offset })
  },
}

export const actions = {
  init({ dispatch }) {
    dispatch('getAllItems')
  },

  getAllItems({ commit, rootState, state: { items } }) {
    // if (!rootState.auth.currentUser) return Promise.resolve(null)
    if (items.length > 0) return Promise.resolve(null)
    return axios
      .get('/api/getAllItems')
      .then(({ data: allItems }) => {
        commit('SET_ITEMS', allItems)
        return allItems
      })
      .catch((error) => {
        if (error.response && error.response.status === 401) {
          commit('SET_ITEMS', null)
        }
        return null
      })
  },

  updateCart(
    {
      state: { items, limit },
      commit,
    },
    [item, index, newAmount]
  ) {
    if (!isNewAmountValid({ stock: item.stock, newAmount })) return

    item.cart = { ...item.cart, amount: newAmount, totalCost: item.price.value * newAmount }
    commit('SET_ITEM', [item, index])
  },
}

export const getters = {
  cart: ({ items }) => {
    return items.reduce(
      (cart, item) => {
        if (item.cart && item.cart.amount > 0) {
          cart.items.push(item)
          cart.totalCost += item.cart.totalCost
          cart.totalItems += item.cart.amount
        }
        return cart
      },
      { items: [], totalItems: 0, totalCost: 0 }
    )
  },
}

// ===
// Private helpers
// ===
const isNewAmountValid = ({ stock, newAmount }) => {
  const isValidAmount = newAmount >= 0
  const hasEnoughItemsStocked = newAmount <= stock
  return hasEnoughItemsStocked && isValidAmount
}
