import { mapState, mapGetters } from 'vuex'

export const cartGetter = {
  ...mapState('store', {
    items: (state) => state.items,
  }),
  ...mapGetters('store', ['cart']),
}
