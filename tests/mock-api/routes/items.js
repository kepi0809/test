const Items = require('../resources/items')

module.exports = (app) => {
  // Log in a user with a username and password
  app.get('/api/getAllItemsCount', (request, response) => {
    const allItemsCount = Items.getAllItemsCount(request.params.token)

    if (!allItemsCount) {
      return response.status(401).json({
        message: 'The token is either invalid or has expired. Please log in again.',
      })
    }

    response.json(allItemsCount)
  })

  app.get('/api/getAllItems', (request, response) => {
    const allItems = Items.getAllItems(request.params.token).then((data) => {
      if (!allItems) {
        return response.status(401).json({
          message: 'The token is either invalid or has expired. Please log in again.',
        })
      }

      response.json(data)
    })
  })
}
